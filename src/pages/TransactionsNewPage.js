import React, { Fragment } from 'react'

import TransactionsForm from '../components/form/TransactionsForm'
import { MainContainer } from '../index.style'
import TitleHeader from '../components/header/TitleHeader'

const TransactionsNewPage = () => (
  <Fragment>
    <TitleHeader title='New transaction' />
    <MainContainer internal>
      <TransactionsForm />
    </MainContainer>
  </Fragment>
)

export default TransactionsNewPage
