import React from 'react'
import { shallow } from 'enzyme'

import HomePage from '../HomePage'

const wrapper = shallow(<HomePage />)

describe('(Component) TransactionsNewPage', () => {
  it('renders without crash', () => {
    expect(wrapper).toHaveLength(1)
  })
})
