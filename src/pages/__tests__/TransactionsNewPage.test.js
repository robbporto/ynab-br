import React from 'react'
import { shallow } from 'enzyme'

import TransactionsNewPage from '../TransactionsNewPage'
import TitleHeader from '../../components/header/TitleHeader'
import TransactionsForm from '../../components/form/TransactionsForm'

const wrapper = shallow(<TransactionsNewPage />)

describe('(Component) TransactionsNewPage', () => {
  it('should have a TitleHeader', () => {
    expect(wrapper.find(TitleHeader).prop('title')).toEqual('New transaction')
  })

  it('should have a Transaction form', () => {
    expect(wrapper.find(TransactionsForm)).toHaveLength(1)
  })
})
