import React, { Fragment } from 'react'

import TransactionsTable from '../components/table/TransactionsTable'
import { MainContainer } from '../index.style'
import TitleHeader from '../components/header/TitleHeader'
import BalanceHeader from '../components/header/BalanceHeader'

const HomePage = () => (
  <Fragment>
    <TitleHeader title='Transactions'>
      <BalanceHeader />
    </TitleHeader>
    <MainContainer internal>
      <TransactionsTable />
    </MainContainer>
  </Fragment>
)

export default HomePage
