import currency from 'currency.js'

export const generateRandomId = () =>
  '_' +
  Math.random()
    .toString(36)
    .substr(2, 9)

export const formatCurrency = value =>
  currency(parseFloat(value), {
    formatWithSymbol: true
  }).format()
