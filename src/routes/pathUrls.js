import Template from '../components/template/Template'

import Home from '../pages/HomePage'
import TransactionsNew from '../pages/TransactionsNewPage'

export const publicRoutes = [
  {
    path: '/',
    component: Home,
    template: Template,
    default: true
  },
  {
    path: '/transactions/new',
    component: TransactionsNew,
    template: Template,
    default: true
  }
]
