import { combineReducers } from 'redux'

import transactionsReducer from './transactions/reducers'

export default combineReducers({
  transactions: transactionsReducer
})
