import { createStore } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'

import storage from 'redux-persist/lib/storage'

import rootReducer from './index'

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['transactions']
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export default ({ initialState } = {}) => {
  let store = createStore(persistedReducer, initialState)
  let persistor = persistStore(store)

  return { store, persistor }
}
