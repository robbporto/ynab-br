import * as actions from '../actions'

describe('transactions actions', () => {
  it('should save a transaction correctly', () => {
    const expected = {
      type: 'transactions:SAVE_TRANSACTION',
      payload: {
        id: 1,
        name: 'Transaction 1',
        value: 10
      }
    }

    expect(
      actions.saveTransaction({
        id: 1,
        name: 'Transaction 1',
        value: 10
      })
    ).toEqual(expected)
  })

  it('should remove the transaction correctly', () => {
    const expected = {
      type: 'transactions:REMOVE_TRANSACTION',
      payload: 1
    }

    expect(actions.removeTransaction(1)).toEqual(expected)
  })
})
