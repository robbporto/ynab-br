import reducer from '../reducers'
import { REMOVE_TRANSACTION, SAVE_TRANSACTION } from '../constants'

describe('transactions reducers', () => {
  const initialState = {
    transactions: []
  }

  it('should return initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState)
  })

  it('should add first transaction', () => {
    const action = {
      type: SAVE_TRANSACTION,
      payload: {
        id: 1,
        created_at: Date.now(),
        name: 'Transaction 1',
        value: 10
      }
    }
    const expectedResponse = {
      transactions: [
        {
          id: 1,
          created_at: Date.now(),
          name: 'Transaction 1',
          value: 10
        }
      ]
    }

    expect(reducer(initialState, action)).toEqual(expectedResponse)
  })

  it('should add one or more transactions', () => {
    const action = {
      type: SAVE_TRANSACTION,
      payload: {
        id: 2,
        created_at: Date.now(),
        name: 'Transaction 2',
        value: 100
      }
    }
    const initialState = {
      transactions: [
        {
          id: 1,
          created_at: Date.now(),
          name: 'Transaction 1',
          value: 10
        }
      ]
    }
    const expectedResponse = {
      transactions: [
        {
          id: 1,
          created_at: Date.now(),
          name: 'Transaction 1',
          value: 10
        },
        {
          id: 2,
          created_at: Date.now(),
          name: 'Transaction 2',
          value: 100
        }
      ]
    }

    expect(reducer(initialState, action)).toEqual(expectedResponse)
  })

  it('should remove a transaction', () => {
    const action = {
      type: REMOVE_TRANSACTION,
      payload: 1
    }
    const initialState = {
      transactions: [
        {
          id: 1,
          created_at: Date.now(),
          name: 'Transaction 1',
          value: 10
        },
        {
          id: 2,
          created_at: Date.now(),
          name: 'Transaction 2',
          value: 100
        }
      ]
    }
    const expectedResponse = {
      transactions: [
        {
          id: 2,
          created_at: Date.now(),
          name: 'Transaction 2',
          value: 100
        }
      ]
    }

    expect(reducer(initialState, action)).toEqual(expectedResponse)
  })
})
