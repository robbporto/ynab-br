import { REMOVE_TRANSACTION, SAVE_TRANSACTION } from './constants'

export const saveTransaction = transaction => ({
  type: SAVE_TRANSACTION,
  payload: transaction
})

export const removeTransaction = transactionId => ({
  type: REMOVE_TRANSACTION,
  payload: transactionId
})
