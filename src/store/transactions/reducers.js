import createReducer from '../createReducer'

import { REMOVE_TRANSACTION, SAVE_TRANSACTION } from './constants'
import { generateRandomId } from '../../utils/functions'

const initialState = {
  transactions: []
}

const transactions = createReducer(initialState, {
  [SAVE_TRANSACTION]: (state, action) => ({
    ...state,
    transactions: [
      ...state.transactions,
      {
        created_at: Date.now(),
        id: generateRandomId(),
        ...action.payload
      }
    ]
  }),
  [REMOVE_TRANSACTION]: (state, action) => ({
    ...state,
    transactions: state.transactions.filter(
      transaction => transaction.id !== action.payload
    )
  })
})

export default transactions
