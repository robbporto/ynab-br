import { injectGlobal } from 'emotion'
import styled from 'react-emotion'

export const DEFAULT_SPACING = 8
export const BORDER_RADIUS = 10
export const BLUE = `#12323C`
export const GRAY = `#aaa`
export const RED = `#f5222d`
export const INPUT_HEIGHT = 50

export const Fieldset = styled('div')`
  margin-bottom: ${DEFAULT_SPACING * 2}px;
  display: flex;
  justify-content: ${props => (props.alignRight ? 'flex-end' : 'flex-start')};
  &:last-of-type {
    margin-bottom: 0;
  }
`

export const ChildFieldset = styled('div')`
  margin-right: ${DEFAULT_SPACING * 2}px;
  flex: 1;
  flex-direction: column;
  &:last-of-type {
    margin-right: 0;
  }
`

export const MainContainer = styled('div')`
  max-width: ${props => (props.internal ? '100%' : '1140px')};
  width: 100%;
  margin: 0 auto;
  padding: ${props =>
    props.internal ? `${DEFAULT_SPACING * 4}px` : `0 ${DEFAULT_SPACING * 4}px`};
  background: ${props => (props.internal ? `white` : `transparent`)};
  border-radius: ${props => (props.internal ? `${BORDER_RADIUS}px` : 0)};
`

injectGlobal`
  *, *:before, *:after {
    box-sizing: border-box;
  }
  
  html, input, textarea, select, button {
    font-family: 'Montserrat', sans-serif;
  }
  
  html {
    font-size: 100%;
  }
  
  body {
    margin: 0;
    padding: 0;
    height: 100%;
    background: #f5f5f5;
  }
  
  h1, h2, h3, h4, h5, h6 {
    margin: 0;
  }
  
  button {
    border: 0;
    background: none;
  }
  ul, ol {
    list-style-type: none;
    margin: 0;
    padding: 0;
  }
  
  a {
    text-decoration: none;
  }
  
  table {
    width: 100%;
    border-collapse: collapse;
    th, td {
      text-align: left;
      padding: 10px;
      margin: 0;
    }
    th {
      border-bottom: 1px solid ${GRAY};
    }
    td {
      padding-top: 20px;
      padding-bottom: 20px;
    }
    tbody tr:nth-child(even) {
      background: #fafafa;
    }
  }

  fieldset {
    border: 0;
    padding: 0;
    width: 100%;
  }
  
  input {
    border-radius: ${BORDER_RADIUS}px;
    height: ${INPUT_HEIGHT}px;
    width: 100%;
    border: 1px solid #d4d4d4;
    padding: 0 ${DEFAULT_SPACING}px;
    font-size: 0.8rem;
  }
  
  .error-message {
    font-size: 0.8rem;
    color: ${RED};
    margin-top: ${DEFAULT_SPACING / 2}px;
  }

`
