import React from 'react'
import { shallow } from 'enzyme'

import EmptyMessageText from '../../text/EmptyMessageText'
import { TransactionsTable } from '../TransactionsTable'
import Button from '../../button/Button'

describe('(Component) TransactionsTable', () => {
  describe("When there's no transactions", () => {
    const wrapper = shallow(
      <TransactionsTable transactions={[]} removeTransaction={jest.fn()} />
    )

    it('should render an EmptyMessage', () => {
      expect(
        wrapper
          .find(EmptyMessageText)
          .children()
          .text()
      ).toEqual('No transactions yet!')
    })
  })

  describe('When transactions exist', () => {
    const props = {
      transactions: [
        {
          created_at: 1544460127423,
          id: '_qm8gvmmwz',
          name: 'Transaction 3',
          value: 30
        },
        {
          created_at: 1544460123737,
          id: '_xnpk4htii',
          name: 'Transaction 2',
          value: 20
        },
        {
          created_at: 1544460081758,
          id: '_ord9wiwc7',
          name: 'Transaction 1',
          value: 10
        }
      ],
      removeTransaction: jest.fn()
    }
    const wrapper = shallow(<TransactionsTable {...props} />)

    it('should have four columns: name, value, partial balance and actions', () => {
      expect(
        wrapper.contains(
          <thead>
            <tr>
              <th>Name</th>
              <th>Value</th>
              <th>Partial balance</th>
              <th>Actions</th>
            </tr>
          </thead>
        )
      ).toBeTruthy()
    })

    it('should render a td containing the name of the transactions', () => {
      expect(
        wrapper.containsMatchingElement(<td>Transaction 1</td>)
      ).toBeTruthy()
    })

    it('should render a td containing the value of the transactions', () => {
      expect(wrapper.containsMatchingElement(<td>$10.00</td>)).toBeTruthy()
    })

    it('should render a td containing the delete transaction button', () => {
      expect(
        wrapper
          .find(Button)
          .at(0)
          .prop('type') === 'delete'
      ).toBeTruthy()
      expect(
        wrapper
          .find(Button)
          .at(0)
          .simulate('click')
      )
      expect(props.removeTransaction).toHaveBeenCalledWith(
        props.transactions[0].id
      )
    })

    it('should render the transactions list according to the created_at attribute (more recent first)', () => {
      expect(
        wrapper
          .find('td')
          .at(0)
          .text()
      ).toEqual('Transaction 3')
    })
  })

  describe('When transactions exist', () => {
    const props = {
      transactions: [
        {
          created_at: 1544460127423,
          id: '_qm8gvmmwz',
          name: 'Transaction 3',
          value: 30
        },
        {
          created_at: 1544460123737,
          id: '_xnpk4htii',
          name: 'Transaction 2',
          value: 20
        },
        {
          created_at: 1544460081758,
          id: '_ord9wiwc7',
          name: 'Transaction 1',
          value: 10
        }
      ],
      removeTransaction: jest.fn()
    }
    const wrapper = shallow(<TransactionsTable {...props} />)

    it('should render the partial balance correctly', () => {
      const firstTr = wrapper.find('tbody tr').at(0)
      const partialBalanceTd = firstTr.find('td').at(2)

      expect(partialBalanceTd.text()).toEqual('$60.00')
    })
  })
})
