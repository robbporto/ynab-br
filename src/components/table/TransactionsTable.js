import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'

import { removeTransaction } from '../../store/transactions/actions'
import EmptyMessageText from '../text/EmptyMessageText'
import Button from '../button/Button'
import { formatCurrency } from '../../utils/functions'

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      removeTransaction
    },
    dispatch
  )

const mapStateToProps = ({ transactions: { transactions } }) => ({
  transactions
})

export const TransactionsTable = ({ transactions, removeTransaction }) => {
  if (transactions.length === 0) {
    return <EmptyMessageText>No transactions yet!</EmptyMessageText>
  }

  const transactionsOrdered = orderTransactionsByInsertion(transactions)

  return (
    <table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Value</th>
          <th>Partial balance</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {transactionsOrdered.map(transaction => (
          <tr key={transaction.id}>
            <td width='40%'>{transaction.name}</td>
            <td width='20%'>{formatCurrency(transaction.value)}</td>
            <td width='20%'>
              {renderPartialBalance(transactionsOrdered, transaction)}
            </td>
            <td width='20%'>
              <Button
                htmlType='button'
                type='delete'
                onClick={() => removeTransaction(transaction.id)}
              >
                Delete transaction
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  )
}

const orderTransactionsByInsertion = transactions =>
  transactions
    .sort(
      (a, b) =>
        new Date(a.created_at).getTime() - new Date(b.created_at).getTime()
    )
    .reverse()

const renderPartialBalance = (transactions, transaction) => {
  const transactionsBefore = transactions
    .filter(t => t.created_at <= transaction.created_at)
    .map(t => t.value)

  return formatCurrency(transactionsBefore.reduce((t1, t2) => t1 + t2, 0))
}

TransactionsTable.propTypes = {
  transactions: PropTypes.array.isRequired,
  removeTransaction: PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionsTable)
