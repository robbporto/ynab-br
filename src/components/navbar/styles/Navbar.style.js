import { css } from 'emotion'
import styled from 'react-emotion'
import { Link } from 'react-router-dom'

import { BLUE, DEFAULT_SPACING } from '../../../index.style'

export const Wrapper = styled('div')`
  height: 90px;
  background: white;
  margin-bottom: ${DEFAULT_SPACING * 8}px;
  box-shadow: 0px 0px 47px -15px rgba(0, 0, 0, 0.75);
`

export const NavbarContainer = css`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 100%;
`

export const LogoNavbar = styled('img')`
  height: 50px;
`

export const Menu = styled('ul')`
  display: flex;
  align-items: center;
  justify-content: center;
`

export const MenuItem = styled('li')`
  margin-right: ${DEFAULT_SPACING * 3}px;
  &:last-of-type {
    margin-right: 0;
  }
`

export const MenuItemLink = styled(Link)`
  color: ${BLUE};
  &:hover {
    text-decoration: underline;
  }
`
