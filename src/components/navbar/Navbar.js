import React from 'react'

import Logo from '../../assets/logo.png'
import {
  LogoNavbar,
  Menu,
  MenuItem,
  MenuItemLink,
  NavbarContainer,
  Wrapper
} from './styles/Navbar.style'
import { MainContainer } from '../../index.style'
import { Link } from 'react-router-dom'

const Navbar = () => (
  <Wrapper>
    <MainContainer css={NavbarContainer}>
      <Link to='/'>
        <LogoNavbar src={Logo} alt='Redux Logo' />
      </Link>
      <Menu>
        <MenuItem>
          <MenuItemLink to='/'>Transactions</MenuItemLink>
        </MenuItem>
        <MenuItem>
          <MenuItemLink to='/transactions/new'>New transaction</MenuItemLink>
        </MenuItem>
      </Menu>
    </MainContainer>
  </Wrapper>
)

export default Navbar
