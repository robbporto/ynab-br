import React from 'react'
import { shallow } from 'enzyme'

import Navbar from '../Navbar'
import {
  LogoNavbar,
  Menu,
  MenuItem,
  MenuItemLink
} from '../styles/Navbar.style'
import Logo from '../../../assets/logo.png'
import { Link } from 'react-router-dom'

describe('(Component) Navbar', () => {
  const wrapper = shallow(<Navbar />)

  it('should render the logo', () => {
    expect(
      wrapper.contains(
        <Link to='/'>
          <LogoNavbar src={Logo} alt='Redux Logo' />
        </Link>
      )
    ).toBeTruthy()
  })

  it('should render the main menu', () => {
    expect(
      wrapper.contains(
        <Menu>
          <MenuItem>
            <MenuItemLink to='/'>Transactions</MenuItemLink>
          </MenuItem>
          <MenuItem>
            <MenuItemLink to='/transactions/new'>New transaction</MenuItemLink>
          </MenuItem>
        </Menu>
      )
    ).toBeTruthy()
  })
})
