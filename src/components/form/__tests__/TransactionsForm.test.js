import React from 'react'
import { shallow } from 'enzyme'

import { TransactionsForm } from '../TransactionsForm'

describe('(Component) TransactionsForm', () => {
  const props = {
    saveTransaction: jest.fn(),
    history: {
      push: jest.fn()
    }
  }

  const wrapper = shallow(<TransactionsForm {...props} />)

  it('should have a field for the name of the transaction', () => {
    expect(wrapper.find('input[name="transactionName"]')).toHaveLength(1)
  })

  it('should have a field for the value of the transaction', () => {
    expect(wrapper.find('input[name="transactionValue"]')).toHaveLength(1)
  })

  it('should save the transaction correctly', () => {
    const transactionName = 'Transaction 1'
    const transactionValue = '1000'

    wrapper
      .find('input[name="transactionName"]')
      .simulate('change', { target: { value: transactionName } })
    wrapper
      .find('input[name="transactionValue"]')
      .simulate('change', { target: { value: transactionValue } })

    wrapper.simulate('submit', { preventDefault: jest.fn() })

    expect(props.saveTransaction).toHaveBeenCalledTimes(1)
    expect(props.history.push).toHaveBeenCalledTimes(1)
  })
})
