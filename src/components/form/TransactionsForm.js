import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators, compose } from 'redux'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'

import { saveTransaction } from '../../store/transactions/actions'
import { ChildFieldset, Fieldset } from '../../index.style'
import Button from '../button/Button'
import { css } from 'emotion'

const FieldsetValue = css`
  flex-basis: 80%;
`

const ContainerButtonSubmit = css`
  justify-content: flex-end;
`

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      saveTransaction
    },
    dispatch
  )

export class TransactionsForm extends Component {
  state = {
    transactionName: '',
    transactionValue: ''
  }

  render () {
    return (
      <form onSubmit={this.handleSubmit}>
        <Fieldset>
          <ChildFieldset className={FieldsetValue}>
            <input
              type='text'
              name='transactionName'
              value={this.state.transactionName}
              onChange={e => this.setState({ transactionName: e.target.value })}
              placeholder='Name'
              required
            />
          </ChildFieldset>
          <ChildFieldset>
            <input
              type='number'
              name='transactionValue'
              value={this.state.transactionValue}
              step='any'
              onChange={e =>
                this.setState({ transactionValue: e.target.value })
              }
              placeholder='Value'
              required
            />
          </ChildFieldset>
        </Fieldset>
        <Fieldset className={ContainerButtonSubmit}>
          <Button htmlType='submit'>Save</Button>
        </Fieldset>
      </form>
    )
  }

  handleSubmit = e => {
    e.preventDefault()

    const { saveTransaction, history } = this.props
    const values = {
      name: this.state.transactionName,
      value: parseFloat(this.state.transactionValue)
    }

    saveTransaction(values)
    history.push('/')
  }
}

const enhancedComponent = compose(connect(null, mapDispatchToProps), withRouter)

TransactionsForm.propTypes = {
  saveTransaction: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func
  }).isRequired
}

export default enhancedComponent(TransactionsForm)
