import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'

import { formatCurrency } from '../../utils/functions'
import { Container, Text } from './styles/BalanceHeader.style'
import PropTypes from 'prop-types'

const mapStateToProps = ({ transactions: { transactions } }) => ({
  transactions
})

export const BalanceHeader = ({ transactions }) => {
  return (
    <Container>
      <small>Balance:</small>
      <Text>
        {transactions.length === 0
          ? formatCurrency(0)
          : renderBalance(transactions)}
      </Text>
    </Container>
  )
}

const transactionsValues = transactions =>
  transactions.map(transaction => transaction.value)
const calculateTotalBalance = transactionsValues =>
  transactionsValues.reduce((acc, current) => acc + current)

const renderBalance = compose(
  formatCurrency,
  calculateTotalBalance,
  transactionsValues
)

BalanceHeader.propTypes = {
  transactions: PropTypes.array.isRequired
}

export default connect(mapStateToProps)(BalanceHeader)
