import styled from 'react-emotion'

export const Container = styled('div')`
  text-align: right;
`

export const Text = styled('div')`
  font-size: 1.5rem;
`
