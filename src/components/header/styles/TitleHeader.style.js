import styled from 'react-emotion'

import { DEFAULT_SPACING } from '../../../index.style'

export const Container = styled('div')`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: ${DEFAULT_SPACING * 3}px;
`

export const Title = styled('div')`
  font-size: 2rem;
  color: black;
`
