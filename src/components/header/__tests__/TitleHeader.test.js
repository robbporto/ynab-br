import React from 'react'
import { shallow } from 'enzyme'

import TitleHeader from '../TitleHeader'
import { Title } from '../styles/TitleHeader.style'

describe('(Component) TitleHeader', () => {
  const props = {
    title: 'Hello, world'
  }

  const wrapper = shallow(
    <TitleHeader {...props}>
      <p>Mock</p>
    </TitleHeader>
  )
  it('should render the title', () => {
    expect(
      wrapper
        .find(Title)
        .dive()
        .text()
    ).toEqual('Hello, world')
  })

  it('should render the children', () => {
    expect(wrapper.find('p').text()).toEqual('Mock')
  })
})
