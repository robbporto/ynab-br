import React from 'react'
import { shallow } from 'enzyme'

import { BalanceHeader } from '../BalanceHeader'
import { Text } from '../styles/BalanceHeader.style'

describe('(Component) BalanceHeader', () => {
  describe("When there's no transactions", () => {
    const wrapper = shallow(
      <BalanceHeader transactions={[]}>lol</BalanceHeader>
    )

    it('should render "$0.00"', () => {
      expect(
        wrapper
          .find(Text)
          .dive()
          .text()
      ).toEqual('$0.00')
    })
  })

  describe('When transactions exist', () => {
    const transactionMock = [
      {
        id: 1,
        name: 'Transaction 1',
        value: 10
      },
      {
        id: 1,
        name: 'Transaction 1',
        value: -20
      }
    ]
    const wrapper = shallow(
      <BalanceHeader transactions={transactionMock}>lol</BalanceHeader>
    )

    it('should render "$-10.00"', () => {
      expect(
        wrapper
          .find(Text)
          .dive()
          .text()
      ).toEqual('-$10.00')
    })
  })
})
