import React from 'react'

import { Container, Title } from './styles/TitleHeader.style'
import PropTypes from 'prop-types'

const TitleHeader = ({ children, title }) => {
  return (
    <Container>
      <Title>{title}</Title>
      {children}
    </Container>
  )
}

TitleHeader.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.func,
    PropTypes.string
  ])
}

export default TitleHeader
