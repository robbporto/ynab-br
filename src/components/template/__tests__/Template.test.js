import React from 'react'
import { shallow } from 'enzyme'

import Template from '../Template'
import Navbar from '../../navbar/Navbar'
import { MainContainer } from '../../../index.style'

describe('(Component) Template', () => {
  const wrapper = shallow(
    <Template>
      <p>Mock</p>
    </Template>
  )

  it('should have a Navbar component', () => {
    expect(wrapper.find(Navbar)).toHaveLength(1)
  })

  it('should have a MainContainer component', () => {
    expect(wrapper.find(MainContainer)).toHaveLength(1)
  })
})
