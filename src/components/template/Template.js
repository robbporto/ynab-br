import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

import Navbar from '../navbar/Navbar'
import { MainContainer } from '../../index.style'

const Template = ({ children }) => (
  <Fragment>
    <Navbar />
    <MainContainer>{children}</MainContainer>
  </Fragment>
)

Template.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.func,
    PropTypes.array
  ]).isRequired
}

export default Template
