import React from 'react'
import { CustomButton } from './styles/Button.style'
import PropTypes from 'prop-types'

const Button = ({ htmlType, onClick, children, type }) => {
  return (
    <CustomButton type={htmlType} uiType={type} onClick={onClick}>
      {children}
    </CustomButton>
  )
}

Button.propTypes = {
  htmlType: PropTypes.string,
  type: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.func,
    PropTypes.string
  ]).isRequired
}

export default Button
