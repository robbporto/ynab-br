import React from 'react'
import { shallow } from 'enzyme'

import Button from '../Button'

describe('(Component) Button', () => {
  const props = {
    htmlType: 'submit',
    type: 'delete',
    onClick: jest.fn()
  }

  const wrapper = shallow(<Button {...props}>lol</Button>)

  it('should have a type', () => {
    expect(wrapper.prop('type')).toEqual('submit')
  })

  it('should have a ui type', () => {
    expect(wrapper.prop('uiType')).toEqual('delete')
  })

  it('should have a onClick event', () => {
    wrapper.simulate('click')
  })
})
