import styled from 'react-emotion'

import { BORDER_RADIUS, INPUT_HEIGHT, RED } from '../../../index.style'

export const CustomButton = styled('button')`
  padding: 0 15px;
  height: ${INPUT_HEIGHT}px;
  background: transparent;
  border: 1px solid ${props => (props.uiType === 'delete' ? RED : '#92caec')};
  color: ${props => (props.uiType === 'delete' ? RED : '#92caec')};
  border-radius: ${BORDER_RADIUS}px;
  font-size: 0.7rem;
  cursor: pointer;
  transition: 0.3s all ease;
  &:hover {
    background-color: ${props => (props.uiType === 'delete' ? RED : '#92caec')};
    color: white;
  }
`
