import React from 'react'

import { Message } from './styles/EmptyMessage.style'
import PropTypes from 'prop-types'

const EmptyMessageText = ({ children }) => {
  return <Message>{children}</Message>
}

EmptyMessageText.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.func,
    PropTypes.string
  ]).isRequired
}

export default EmptyMessageText
