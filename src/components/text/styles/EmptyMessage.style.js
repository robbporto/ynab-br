import styled from 'react-emotion'

import { DEFAULT_SPACING, GRAY } from '../../../index.style'

export const Message = styled('div')`
  font-size: 1.5rem;
  color: ${GRAY};
  text-align: center;
  margin: ${DEFAULT_SPACING * 5}px;
`
