import React from 'react'
import { shallow } from 'enzyme'

import EmptyMessageText from '../EmptyMessageText'

describe('(Component) EmptyMessageText', () => {
  const wrapper = shallow(
    <EmptyMessageText>
      <div>Empty message</div>
    </EmptyMessageText>
  )

  it('should render the children', () => {
    expect(wrapper.find('div').text()).toEqual('Empty message')
  })
})
